# Frankenbox

A pet project to convert a CEL Robox to a RAMPS/E3D v6 system.

Gracious thanks to Prusa Research for releasing printed parts to the Prusa i3 MK2 under the GNU GPL V2 so they can be modified for projects such as this. You can find the original source files at https://github.com/prusa3d/Original-Prusa-i3/.

This project is not affiliated with or endorsed by CEL, nor is it guaranteed to function correctly or safely. If you decide to do something similar, you do so at your own risk.